# README #


### What is this repository for? ###

This repo holds the source code of TNC China iOS app


### How do I get set up? ###

This project needs Xcode 8.0+ and uses Swift 3. Cocoapods is used for dependency management. In order to run this project on your machine, use the following steps.

1. Install Cocoapod on your Mac
2. cd to your directory and run "pod install"
3. Double click the "tncchina.xcworkspace" to open the project in Xcode

### Contribution guidelines ###

Please stick with style guide in 
https://github.com/raywenderlich/swift-style-guide