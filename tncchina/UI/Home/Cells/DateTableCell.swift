//
//  DateTableCell.swift
//  tncchina
//
//  Created by Anson Yao on 2017-06-08.
//  Copyright © 2017 TNC. All rights reserved.
//

import UIKit
import SnapKit

class DateTableCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setup(date: Date, weather: Weather, temperature: Double) {
        for view in subviews {
            if view is DateTableCellView {
                view.removeFromSuperview()
            }
        }
        let innerView = Bundle.main.loadNibNamed("DateTableCellView", owner: nil, options: nil)![0] as! DateTableCellView
        addSubview(innerView)
        innerView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.snp.edges)
        }
    }
    
}
