//
//  EventCollectionViewCell.swift
//  tncchina
//
//  Created by Anson Yao on 2017-06-08.
//  Copyright © 2017 TNC. All rights reserved.
//

import UIKit

class EventCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var topLeftImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    ///The imageview beneath the option button
    @IBOutlet weak var optionImageView: UIImageView!
    @IBOutlet weak var optionButton: UIButton!
    @IBOutlet weak var contentImageView: UIImageView!
    @IBOutlet weak var descriptionMaskContainer: UIView!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var interestedInButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setup() {
        
    }
}
