//
//  EventCalendarView.swift
//  tncchina
//
//  Created by Anson Yao on 2017-06-08.
//  Copyright © 2017 TNC. All rights reserved.
//

import UIKit

class EventCalendarView: UIView {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!

    func setup() {
    
    }

}
