//
//  DateTableCellView.swift
//  tncchina
//
//  Created by Anson Yao on 2017-06-08.
//  Copyright © 2017 TNC. All rights reserved.
//

import UIKit
import SwiftDate

class DateTableCellView: UIView {
    @IBOutlet weak var weekdayLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var weatherIconImageView: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setup(date: Date, weather: Weather, temperature: Double) {
        weekdayLabel.text = date.weekdayName
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd"
        dateLabel.text = formatter.string(from: date)
        weatherIconImageView.image = weather.image
        temperatureLabel.text = String(format: "%.0d", temperature)
    }

}
