//
//  Weather.swift
//  tncchina
//
//  Created by Anson Yao on 2017-06-08.
//  Copyright © 2017 TNC. All rights reserved.
//

import UIKit

enum Weather: String {
    case sunny = "Sunny",  cloudy = "Cloudy",  rain = "Rain", snow = "Snow"
    
    var image: UIImage {
        switch self {
        case .sunny:
            return #imageLiteral(resourceName: "sunny")
        case .cloudy:
            return #imageLiteral(resourceName: "cloudy")
        case .rain:
            return #imageLiteral(resourceName: "rain")
        case .snow:
            return #imageLiteral(resourceName: "snowflake")
        }
    }
}
